absl-py==1.1.0
alembic==1.8.0
anyio==3.6.1
appnope==0.1.3
aquirdturtle-collapsible-headings==3.1.0
argon2-cffi==21.3.0
argon2-cffi-bindings==21.2.0
asn1crypto==1.5.1
asttokens==2.0.5
astunparse==1.6.3
attrs==21.4.0
AutoTS==0.4.1
Babel==2.10.3
backcall==0.2.0
beautifulsoup4==4.11.1
black==22.3.0
bleach==5.0.1
cachetools==5.2.0
certifi==2022.6.15
cffi==1.15.1
charset-normalizer==2.0.12
click==8.1.2
cloudpickle==2.1.0
colorama==0.4.5
cryptography==36.0.2
cycler==0.11.0
Cython==0.29.30
dask==2022.6.0
databricks-cli==0.17.0
debugpy==1.6.0
decorator==5.1.1
defusedxml==0.7.1
docker==5.0.3
eli5==0.13.0
entrypoints==0.4
executing==0.8.3
fastdist==1.1.3
fastjsonschema==2.15.3
Flask==2.1.2
flatbuffers==1.12
fonttools==4.33.3
fsspec==2022.5.0
gast==0.4.0
gitdb==4.0.9
gitlabdata==0.3.15
gitlabds==1.0.16
GitPython==3.1.27
google-api-core==2.8.2
google-auth==2.9.0
google-auth-oauthlib==0.4.6
google-cloud-core==2.3.1
google-cloud-secret-manager==2.11.1
google-cloud-storage==2.4.0
google-crc32c==1.3.0
google-pasta==0.2.0
google-resumable-media==2.3.3
googleapis-common-protos==1.56.3
graphviz==0.20
greenlet==1.1.2
grpc-google-iam-v1==0.12.4
grpcio==1.47.0
grpcio-status==1.47.0
gunicorn==20.1.0
h5py==3.7.0
htmlmin==0.1.12
idna==3.3
ImageHash==4.2.1
imbalanced-learn==0.9.1
importlib-metadata==4.12.0
importlib-resources==5.8.0
ipykernel==6.15.0
ipython==8.4.0
ipython-genutils==0.2.0
ipywidgets==7.7.0
isort==5.10.1
itsdangerous==2.1.2
jedi==0.18.1
Jinja2==3.1.2
joblib==1.1.0
json5==0.9.8
jsonschema==4.6.1
jupyter-client==7.3.4
jupyter-core==4.10.0
jupyter-resource-usage==0.6.1
jupyter-server==1.18.0
jupyter-server-mathjax==0.2.5
jupyterlab==3.4.3
jupyterlab-code-formatter==1.4.11
jupyterlab-execute-time==2.1.0
jupyterlab-git==0.37.1
jupyterlab-pygments==0.2.2
jupyterlab-server==2.14.0
jupyterlab-system-monitor==0.8.0
jupyterlab-templates==0.3.1
jupyterlab-topbar==0.6.1
jupyterlab-widgets==1.1.1
keras==2.9.0
Keras-Preprocessing==1.1.2
kiwisolver==1.4.3
lckr-jupyterlab-variableinspector==3.0.9
libclang==14.0.1
line-profiler==3.5.1
llvmlite==0.38.1
locket==1.0.0
Mako==1.2.1
Markdown==3.3.7
MarkupSafe==2.1.1
matplotlib==3.5.2
matplotlib-inline==0.1.3
memory-profiler==0.60.0
missingno==0.5.1
mistune==0.8.4
mlflow==1.26.1
modin==0.15.1
mpmath==1.2.1
multimethod==1.8
mypy-extensions==0.4.3
nbclassic==0.4.0
nbclient==0.6.6
nbconvert==6.5.0
nbdime==3.1.1
nbformat==5.4.0
nest-asyncio==1.5.5
networkx==2.8.4
notebook==6.4.12
notebook-shim==0.1.0
numba==0.55.2
numpy==1.22.4
oauthlib==3.2.0
opt-einsum==3.3.0
oscrypto==1.3.0
packaging==21.3
pandas==1.4.2
pandas-profiling==3.2.0
pandocfilters==1.5.0
parso==0.8.3
partd==1.2.0
pathspec==0.9.0
patsy==0.5.2
pexpect==4.8.0
phik==0.12.2
pickleshare==0.7.5
Pillow==9.2.0
platformdirs==2.5.2
plotly==5.8.2
prometheus-client==0.14.1
prometheus-flask-exporter==0.20.2
prompt-toolkit==3.0.30
proto-plus==1.20.6
protobuf==3.19.4
psutil==5.9.1
ptyprocess==0.7.0
pure-eval==0.2.2
pyasn1==0.4.8
pyasn1-modules==0.2.8
pycparser==2.21
pycryptodomex==3.15.0
pydantic==1.9.1
Pygments==2.12.0
PyJWT==2.4.0
pyodbc==4.0.32
pyOpenSSL==22.0.0
pyparsing==3.0.9
pyrsistent==0.18.1
python-dateutil==2.8.2
python-dotenv==0.20.0
pytz==2022.1
PyWavelets==1.3.0
PyYAML==5.4.1
pyzmq==23.2.0
querystring-parser==1.2.4
requests==2.28.1
requests-oauthlib==1.3.1
rsa==4.8
scikit-learn==1.1.1
scipy==1.8.1
seaborn==0.11.2
Send2Trash==1.8.0
shap==0.40.0
six==1.16.0
slicer==0.0.7
smmap==5.0.0
sniffio==1.2.0
snowflake-connector-python==2.7.8
snowflake-sqlalchemy==1.3.4
soupsieve==2.3.2.post1
SQLAlchemy==1.4.39
sqlparse==0.4.2
stack-data==0.3.0
statsmodels==0.13.2
sympy==1.10.1
tabulate==0.8.10
tangled-up-in-unicode==0.2.0
tenacity==8.0.1
tensorboard==2.9.1
tensorboard-data-server==0.6.1
tensorboard-plugin-wit==1.8.1
tensorflow==2.9.1
tensorflow-estimator==2.9.0
tensorflow-io-gcs-filesystem==0.26.0
termcolor==1.1.0
terminado==0.15.0
threadpoolctl==3.1.0
tinycss2==1.1.1
tokenize-rt==4.2.1
tomli==2.0.1
toolz==0.11.2
tornado==6.2
tqdm==4.64.0
traitlets==5.3.0
typing_extensions==4.2.0
urllib3==1.26.9
visions==0.7.4
wcwidth==0.2.5
webencodings==0.5.1
websocket-client==1.3.3
Werkzeug==2.1.2
widgetsnbextension==3.6.1
wrapt==1.14.1
xgboost==1.6.1
zipp==3.8.0
papermill==2.3.4
